﻿using LibraryBooksEF.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryBooksEF.Data
{
    public class LibraryContext:DbContext
    {
        public LibraryContext() : base()
        {
            Database.EnsureCreated();
        }

        public DbSet<Author> Authors { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Reader> Reader { get; set; }
        public DbSet<AuthorBook> AuthorBooks { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=DESKTOP-0V33MK0\\MSSQLSERVER01; Database = LibraryBooksEF; Trusted_Connection=true;");
        }
    }
}
