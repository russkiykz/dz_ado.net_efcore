﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryBooksEF.Models
{
    public class Reader:Entity
    {
        public string FullName { get; set; }
        public bool Debt { get; set; }
        public ICollection<Book> Books { get; set; }
    }
}
