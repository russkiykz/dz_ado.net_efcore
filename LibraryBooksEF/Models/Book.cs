﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryBooksEF.Models
{
    public class Book:Entity
    {
        public string Name { get; set; }
        public ICollection<AuthorBook> AuthorBooks { get; set; }
    }
}
