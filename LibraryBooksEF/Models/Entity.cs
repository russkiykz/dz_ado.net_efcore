﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryBooksEF.Models
{
    public class Entity
    {
        public Guid Id { get; private set; } = Guid.NewGuid();
    }
}
