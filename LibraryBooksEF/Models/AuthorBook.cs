﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryBooksEF.Models
{
    public class AuthorBook:Entity
    {
        public Guid AuthorId { get; set; }
        public Author Author { get; set; }
        public Guid BookId { get; set; }
        public Book Book { get; set; }
    }
}
