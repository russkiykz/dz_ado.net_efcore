﻿using LibraryBooksEF.Data;
using LibraryBooksEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LibraryBooksEF.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            //var authors = new List<Author>()
            //{
            //    new Author() { Name = "А.Дюма" },
            //    new Author() { Name = "И.Ильф" },
            //    new Author() { Name = "Е.Петров" },
            //    new Author() { Name = "С.Лукьяненко" },
            //    new Author() { Name = "В.Васильев" },
            //    new Author() { Name = "С.Кинг" },
            //    new Author() { Name = "Л.Пантелеев" },
            //    new Author() { Name = "Г.Белых" }
            //};

            //var books = new List<Book>()
            //{
            //    new Book() {Name="Три Мушкетера"},
            //    new Book() {Name="Двенадцать стульев"},
            //    new Book() {Name="Дневной дозор" },
            //    new Book() {Name="Зеленая миля"},
            //    new Book() {Name="Республика Шкид"}
            //};

            //var authorBooks = new List<AuthorBook>()
            //{
            //    new AuthorBook(){Author=authors[0],Book=books[0]},
            //    new AuthorBook(){Author=authors[1],Book=books[1]},
            //    new AuthorBook(){Author=authors[2],Book=books[1]},
            //    new AuthorBook(){Author=authors[3],Book=books[2]},
            //    new AuthorBook(){Author=authors[4],Book=books[2]},
            //    new AuthorBook(){Author=authors[5],Book=books[3]},
            //    new AuthorBook(){Author=authors[6],Book=books[4]},
            //    new AuthorBook(){Author=authors[7],Book=books[4]}
            //};

            //var readers = new List<Reader>()
            //{
            //    new Reader(){FullName="Никита",Debt=true, Books=new List<Book>(){books[0] } },
            //    new Reader(){FullName="Абай",Debt=false,Books=new List<Book>(){books[1] } },
            //    new Reader(){FullName="Олег",Debt=false, Books=new List<Book>(){books[2] } },
            //    new Reader(){FullName="Ержан",Debt=true,Books=new List<Book>(){books[3] } }
            //};

            //using (var context = new LibraryContext())
            //{
            //    foreach (var reader in readers)
            //    {
            //        context.Add(reader);
            //    }
            //    foreach (var author in authors)
            //    {
            //        context.Add(author);
            //    }
            //    foreach (var authorBook in authorBooks)
            //    {
            //        context.Add(authorBook);
            //    }
            //    context.SaveChanges();
            //}

            using (var context = new LibraryContext())
            {
                var books = context.Books.ToList();
                var authors = context.Authors.ToList();
                var readers = context.Reader.ToList();
                var authorBooks = context.AuthorBooks.ToList();

                //1.Выведите список должников.
                Console.WriteLine("Список должников: ");
                foreach (var reader in readers)
                {
                    if (reader.Debt == true)
                    {
                        Console.WriteLine($"{reader.FullName}");
                    }
                }

                //2.Выведите список авторов книги №3(по порядку из таблицы ‘Book’).
                Console.WriteLine("\nСписок авторов книги №3: ");
                foreach (var authorBook in authorBooks)
                {
                    if (books[2].Id == authorBook.BookId)
                    {
                        Console.WriteLine(authorBook.Author.Name);
                    }
                }

                //3.Выведите список книг, которые доступны в данный момент.
                
                // Возможно из-за резализации БД не могу понять как этот запрос выполнить...

                // 4. Вывести список книг, которые на руках у пользователя №2.
                Console.WriteLine("\nСписок книг на руках у пользователя №2: ");
                foreach (var book in readers[1].Books)
                {
                    Console.WriteLine(book.Name);
                }


                // 5. Обнулите задолженности всех должников.
                foreach (var reader in readers)
                {
                    if (reader.Debt == true)
                    {
                        reader.Debt = false;
                        context.Reader.Update(reader);
                    }
                }
                context.SaveChanges();
                

            }


        }
    }
}
